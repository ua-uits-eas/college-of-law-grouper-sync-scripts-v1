# UA College of Law Grouper Sync

This project consists of a master shell script (`lawsync.sh`), which defines some global variables (e.g., credentials, Grouper hostname) and invokes three additional shell scripts. These shell scripts, in turn, each execute a Python script, which performs a synchronization task with Grouper--typically based on an EDS query. Finally, the master shell script invokes an additional shell script that uploads an audit file to a Box.com folder.

Info on each script provided below.

### `lawsync.sh` ###
+ master shell script..this is what you run to kick off the whole sync process
+ environment variables:
    + `EDS_USER` -- EDS app account username; used for querying both EDS and Grouper
    + `EDS_PW` -- EDS app account password
    + `EDS_BASE_DN` (default: _dc=eds,dc=arizona,dc=edu_)
    + `EDS_HOST` (default: _ldaps://eds.arizona.edu_)
    + `GROUPER_WS_HOST` (default: _grouper.arizona.edu_)
    + `EMAIL_ERR_SENDER` -- 'From:' address for sending error emails (default: _uits-sia@list.arizona.edu_)
    + `EMAIL_ERR_RCPTS` -- comma-separated list of 'To:' addresses for receiving error emails
    + `EMAIL_SMTP_GATEWAY` -- SMTP relay for sending error emails (default: smtpgate.email.arizona.edu)
    + `REQUESTS_CA_BUNDLE` -- PEM-formatted bundle of CA certificates (intermediates + root) needed for Grouper https (default:     _ca_chain.pem_)

### `law_common.py` ###
+ common definitions and functions
+ included in other Python scripts

### `law-empdcc.sh` ###
+ executes Python scripts related to employees and DCCs:
    + `law-employees.py`
    + `law-studentworkers.py`
    + `law-dcc.py`
+ environment variables:
    + `GROUPER_BASE_PATH` (default: _/grouper-ws/servicesRest/json/v2_2_001/groups/_)
    + `GROUPER_STEM` (default: _arizona.edu:dept:LAW_)

### `lawclinic-employees.sh` ###
+ executes Python scripts related to employees teaching Law Clinics:
    + `lawclinic-employees.py`
+ environment variables:
    + `GROUPER_BASE_PATH` (default: _/grouper-ws/servicesRest/json/v2_2_001_)
    + `GROUPER_GROUP_NAME` (default: _arizona.edu:dept:LAW:lawclinicemployees_)
    + `GROUPER_ATTR_NAME` (default: _arizona.edu:dept:LAW:memberterm_)
    + `SOC_WS_URL` (default: _https://ws.uits.arizona.edu/UA_Courses/crsdetail_)
    + `TERM_DATES_WS_URL` (default: _https://ws.uits.arizona.edu/TermDates_)

### `lawclinic-students.sh` ###
+ executes Python scripts related to students enrolled in Law Clinics:
    + `lawclinic-students.py`
+ environment variables:
    + `GROUPER_BASE_PATH` (default: _/grouper-ws/servicesRest/json/v2_2_001_)
    + `GROUPER_GROUP_NAME` (default: _arizona.edu:dept:LAW:lawclinicstudents_)
    + `GROUPER_ATTR_NAME` (default: _arizona.edu:dept:LAW:memberterm_)
    + `SOC_WS_URL` (default: _https://ws.uits.arizona.edu/UA_Courses/crsdetail_)
    + `TERM_DATES_WS_URL` (default: _https://ws.uits.arizona.edu/TermDates_)

### `law-audit-upload.sh` ###
+ executes Python script to upload audit file to Box folder:
    + `law-audit-upload.py`
+ environment variables:
    + `BOX_USER` -- Box.com "external access" credential username
    + `BOX_PASS` -- Box.com "external access" credential password
    + `BOX_DIR` -- path to Box folder in which to place audit file (relative to `/`)
    + `FTP_HOST` -- hostname of SFTP server (default: _ftp.box.com_)
