#!/usr/bin/python

import os
import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from datetime import datetime

def send_email(send_from, send_to, subject, text, files=None,
              server="127.0.0.1"):

    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(text))

    for f in files or []:
        with open(f['file'], "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=f['name']
            )
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="%s"' % f['name']
        msg.attach(part)

    smtp = smtplib.SMTP(server)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()

subject = os.environ['AUDIT_EMAIL_SUBJ']
send_from = os.environ['AUDIT_EMAIL_FROM']
send_to = os.environ['AUDIT_EMAIL_RCPT']
smtp_host = os.environ['SMTP_HOST']
send_email(send_from, send_to, subject, 'Law Grouper Sync Audit File attached.', [{'file': 'audit.csv', 'name': "audit-" + datetime.now().strftime("%Y%m%d%H%M%S") + ".csv"}], smtp_host)
