#!/bin/sh

cd $SCRIPT_DIR

# Grouper WS stuff
export AUDIT_EMAIL_SUBJ='Law Grouper Sync audit file'
export AUDIT_EMAIL_FROM='uits-sia@list.arizona.edu'
export AUDIT_EMAIL_RCPT='windhamg@email.arizona.edu'
export SMTP_HOST='smtpgate.email.arizona.edu'

python ${SCRIPT_DIR}/law-audit-email.py

rm -f audit.csv
