#!/usr/bin/python

import os
from ftplib import FTP_TLS
from datetime import datetime

# EDS connection paramaters
ftp_host = os.environ['FTP_HOST']
box_user = os.environ['BOX_USER']
box_pass = os.environ['BOX_PASS']
box_dir = os.environ['BOX_DIR']

ftps = FTP_TLS(ftp_host, box_user, box_pass)
ftps.cwd(box_dir)
f = open('audit.csv','r')
fname = "audit-" + datetime.now().strftime("%Y%m%d%H%M%S") + ".csv"
ftps.storlines('STOR ' + fname, f)
f.close()
ftps.quit()
