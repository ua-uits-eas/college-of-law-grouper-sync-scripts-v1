#!/bin/sh

cd $SCRIPT_DIR

# Grouper WS stuff
export BOX_USER="user@email.arizona.edu"
export BOX_PASS='<PASSWORD>'
export BOX_DIR='<dir>'
export FTP_HOST='ftp.box.com'

python ${SCRIPT_DIR}/law-audit-upload.py

rm -f audit.csv
