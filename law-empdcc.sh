#!/bin/sh

# Grouper WS stuff
export GROUPER_BASE_PATH="/grouper-ws/servicesRest/json/v2_2_001/groups/"
export GROUPER_STEM='arizona.edu:dept:LAW'

python ${SCRIPT_DIR}/law-employees.py
if [ $? -ne 0 ]; then
  echo "law-employees.py exited with error"
  exit 1
fi
python ${SCRIPT_DIR}/law-studentworkers.py
if [ $? -ne 0 ]; then
  echo "law-studentworkers.py exited with error"
  exit 1
fi
python ${SCRIPT_DIR}/law-dcc.py
if [ $? -ne 0 ]; then
  echo "law-dcc.py exited with error"
  exit 1
fi
