#!/usr/bin/python

import os
import re
import ldap
import requests
from lawcommon import logEvent, send_err_email

# EDS connection paramaters
eds_user = os.environ['EDS_USER']
eds_base_dn = os.environ['EDS_BASE_DN']
eds_bind_dn = "uid=" + eds_user + ",ou=app users," + eds_base_dn
eds_bind_pw = os.environ['EDS_PW']
eds_search_dn = "ou=people," + eds_base_dn
eds_host = os.environ['EDS_HOST']

# Grouper params
grouper_host = os.environ['GROUPER_WS_HOST']
grouper_base_path = os.environ['GROUPER_BASE_PATH']
grouper_stem = os.environ['GROUPER_STEM']
grouper_user = eds_user
grouper_pass = eds_bind_pw

# populate law dept groups
for dept in ['3601', '3602', '3603']:
    try:
        r = requests.get("https://%s%s%s/members" % (grouper_host, grouper_base_path, grouper_stem + ":emp" + dept), auth=(grouper_user, grouper_pass))
        grouper_resp = r.json()
    except requests.exceptions.RequestException as e:
        print(e)
        exit(1)

    grouper_members = []
    if 'wsSubjects' in grouper_resp['WsGetMembersLiteResult']:
        for rec in grouper_resp['WsGetMembersLiteResult']['wsSubjects']:
            if re.search('^\d{12}$', rec['id']) and rec['sourceId'] == "ldap":
                grouper_members.append(rec['id'])

    # get eligible employees from EDS
    ld = ldap.initialize(eds_host)
    ld.simple_bind_s(eds_bind_dn, eds_bind_pw)
    eds_members = []
    for e in ld.search_s("ou=people," + eds_base_dn, ldap.SCOPE_SUBTREE,
                         "(&(!(objectclass=arizonaEduTestPerson))(|(eduPersonAffiliation=retiree)(eduPersonAffiliation=staff)(eduPersonAffiliation=faculty))(|(employeePrimaryDept=%s)(&(employeePositionFunding=*:%s)(employeeIncumbentPosition=*))))" % (dept, dept), ['uaid', 'employeePrimaryDept', 'employeeIncumbentPosition', 'employeePositionFunding']):
        if e[1]['employeePrimaryDept'][0] == dept:
            eds_members.append(e[1]['uaid'][0])
        else:
            for i in e[1]['employeeIncumbentPosition']:
                incParts = i.split(':')
                pos = incParts[1]
                status = incParts[4]
                if status not in ['A', 'W', 'R']:
                    continue
                for pf in e[1]['employeePositionFunding']:
                    pfParts = pf.split(':')
                    if pfParts[1] == dept:
                        eds_members.append(e[1]['uaid'][0])

    # get "pending new hires" from EDS
    for e in ld.search_s("ou=people," + eds_base_dn, ldap.SCOPE_SUBTREE,
                         "(&(!(objectclass=arizonaEduTestPerson))(|(edupersonaffiliation=affiliate)(edupersonaffiliation=dcc))(dccRelation=*:00995:%s:*))" % dept, ['uaid']):
            eds_members.append(e[1]['uaid'][0])

    # check eds_members length and exit w/ error email if it is too low
    cnt_ok = False
    try:
        fo = open("law-employees-%s.cnt" % dept, "r+")
        old_cnt = int(fo.readline().strip())
    except IOError as e:
        fo = open("law-employees-%s.cnt" % dept, "w")
        old_cnt = 0
    new_cnt = len(eds_members)
    if new_cnt >= (0.97 * old_cnt):
        cnt_ok = True
        fo.seek(0, 0)
        fo.truncate(0)
        fo.write(str(new_cnt))
        fo.close()
    if not cnt_ok:
        send_err_email("alert: error in law-employees-%s count: old count = %d, new count = %d" %
                       (dept, old_cnt, new_cnt))
        exit(1)

    # delete all members from Grouper not in EDS results
    del_mems = set(grouper_members) - set(eds_members)
    for m in del_mems:
        try:
            r = requests.delete("https://%s%s%s/members/%s" % (grouper_host, grouper_base_path, grouper_stem + ":emp" + dept, m), auth=(grouper_user, grouper_pass))
            grouper_resp = r.json()
            if grouper_resp['WsDeleteMemberLiteResult']['resultMetadata']['resultCode'] not in ('SUCCESS', 'SUCCESS_WASNT_IMMEDIATE_BUT_HAS_EFFECTIVE'):
                print("Error deleting %s from group %s : %s" % (m, grouper_resp['WsDeleteMemberLiteResult']['wsGroup']['name'], grouper_resp['WsDeleteMemberLiteResult']['resultMetadata']['resultCode']))
                exit(1)
            else:
                logEvent('DEL', grouper_resp['WsDeleteMemberLiteResult']['wsGroup']['name'], m)
        except requests.exceptions.RequestException as e:
            print(e)
            exit(1)

    # add all missing members to Grouper
    add_mems = set(eds_members) - set(grouper_members)
    for m in add_mems:
        try:
            r = requests.put("https://%s%s%s/members/%s" % (grouper_host, grouper_base_path, grouper_stem + ":emp" + dept, m), auth=(grouper_user, grouper_pass))
            grouper_resp = r.json()
            if grouper_resp['WsAddMemberLiteResult']['resultMetadata']['resultCode'] not in ('SUCCESS', 'SUCCESS_ALREADY_EXISTED', 'SUCCESS_WASNT_IMMEDIATE_BUT_HAS_EFFECTIVE'):
                print("Error adding %s to group %s : %s" % (m, grouper_resp['WsAddMemberLiteResult']['wsGroupAssigned']['name'], grouper_resp['WsAddMemberLiteResult']['resultMetadata']['resultCode']))
                exit(1)
            else:
                logEvent('ADD', grouper_resp['WsAddMemberLiteResult']['wsGroupAssigned']['name'], m)
        except requests.exceptions.RequestException as e:
            print(e)
            exit(1)
