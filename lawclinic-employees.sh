#!/bin/sh

# Grouper WS stuff
export GROUPER_BASE_PATH="/grouper-ws/servicesRest/json/v2_2_001"
export GROUPER_GROUP_NAME="arizona.edu:dept:LAW:lawclinicemployees"
export GROUPER_ATTR_NAME="arizona.edu:dept:LAW:memberterm"

# schedule/dates web services
export SOC_WS_URL="https://ws.uits.arizona.edu/UA_Courses/crsdetail"
export TERM_DATES_WS_URL="https://ws.uits.arizona.edu/TermDates"

python ${SCRIPT_DIR}/lawclinic-employees.py
if [ $? -ne 0 ]; then
  echo "lawclinic-employees.py exited with error"
  exit 1
fi
