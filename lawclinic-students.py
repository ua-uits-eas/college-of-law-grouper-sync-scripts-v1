#!/usr/bin/python

import os
import re
import ldap
import requests
from lawcommon import logEvent, send_err_email
from datetime import date, datetime, timedelta
from xml.etree import ElementTree

# EDS connection paramaters
eds_user = os.environ['EDS_USER']
eds_base_dn = os.environ['EDS_BASE_DN']
eds_bind_dn = "uid=" + eds_user + ",ou=app users," + eds_base_dn
eds_bind_pw = os.environ['EDS_PW']
eds_search_dn = "ou=people," + eds_base_dn
eds_host = os.environ['EDS_HOST']

# Grouper params
grouper_host = os.environ['GROUPER_WS_HOST']
grouper_base_path = os.environ['GROUPER_BASE_PATH']
grouper_groupname = os.environ['GROUPER_GROUP_NAME']
grouper_attr_name = os.environ['GROUPER_ATTR_NAME']
grouper_user = eds_user
grouper_pass = eds_bind_pw

# Student WS params
soc_url = os.environ['SOC_WS_URL']
terms_url = os.environ['TERM_DATES_WS_URL']


# determine terms we're looking for
# namespace for UA_Courses XML response
ns = {'ns1': 'http://uamobile.arizona.edu/schema/UA_Courses'}
dates = [date.today()]  # today ### THIS IS THE REAL CODE
# dates = [ datetime.strptime("20150830", "%Y%m%d") ] # FOR TESTING
dates.append((date.today() - timedelta(days=28)))  # 4 weeks ago
dates.append((date.today() + timedelta(days=14)))  # 2 weeks in future
searchTerms = {}

for d in dates:
    r = requests.get(terms_url, params={'date': d.strftime("%Y%m%d")})
    termsJson = r.json()
    for key in termsJson:
        searchTerms[key] = {
            'start': termsJson[key]['start'], 'end': termsJson[key]['end']}

sections = {}
eds_members = {}
grouper_members = {}

for clinic in [{'subject_code': 'LAW', 'catalog_nbr': '696C'}, {'subject_code': 'LAW', 'catalog_nbr': '696D'}, {'subject_code': 'LAW', 'catalog_nbr': '643K'}]:
    for term in searchTerms:
        try:
            r = requests.get(soc_url, params={'subject_code': clinic[
                             'subject_code'], 'catalog_nbr': clinic['catalog_nbr'], 'term_code': term})
            tree = ElementTree.fromstring(r.text)
            if len(tree):  # is this an empty response?
                for crsdtl in tree[0].iterfind('ns1:CourseDetail', ns):
                    start = crsdtl.find(
                        './ns1:MeetingCollection/ns1:Meeting/ns1:start_date', ns).text
                    end = crsdtl.find(
                        './ns1:MeetingCollection/ns1:Meeting/ns1:end_date', ns).text
                    if start and end:  # are these empty elements?
                        section = {'start': datetime.strptime(
                            start, "%m/%d/%Y"), 'end': datetime.strptime(end, "%m/%d/%Y")}
                    else:
                        section = {'start': datetime.strptime(searchTerms[term][
                                                              'start'], "%Y%m%d"), 'end': datetime.strptime(searchTerms[term]['end'], "%Y%m%d")}
                    sections[term + ':' + clinic['subject_code'] + ':' + clinic[
                        'catalog_nbr'] + ':' + crsdtl.find('ns1:section', ns).text] = section

        except requests.exceptions.RequestException as e:
            print(e)
            exit(1)

        # get eligible members from EDS
        ld = ldap.initialize(eds_host)
        ld.simple_bind_s(eds_bind_dn, eds_bind_pw)
        for k, v in sections.iteritems():
            term, subj, catnbr, sect = k.split(':')
            # GDW -- 2016/01/06 -- base eligiblilty on end-date only, not start date
            # if datetime.today() >= v['start'] and datetime.today() < (v['end'] + timedelta(days=14)):
            if datetime.today() < (v['end'] + timedelta(days=28)):
                for e in ld.search_s("ou=people," + eds_base_dn, ldap.SCOPE_SUBTREE,
                                     "(&(edupersonaffiliation=student)(!(objectclass=arizonaEduTestPerson))(isMemberOf=arizona.edu:course:%s:*:%s:%s:%s:*:learner))" % (term, subj, catnbr, sect), ['uaid']):
                    uaid = e[1]['uaid'][0]
                    if (uaid not in eds_members) or (v['end'] > eds_members[uaid]['endDate']):
                        eds_members[uaid] = {
                            'term': term, 'endDate': v['end']}

# check eds_members length and exit w/ error email if it is too low
cnt_ok = False
try:
    fo = open("lawclinic-students.cnt", "r+")
    old_cnt = int(fo.readline().strip())
except IOError as e:
    fo = open("lawclinic-students.cnt", "w")
    old_cnt = 0
new_cnt = len(eds_members)
if new_cnt >= (0.97 * old_cnt):
    cnt_ok = True
    fo.seek(0, 0)
    fo.truncate(0)
    fo.write(str(new_cnt))
    fo.close()
if not cnt_ok:
    send_err_email("alert: error in lawclinic-students count: old count = %d, new count = %d" %
                   (old_cnt, new_cnt))
    exit(1)

try:
    r = requests.get("https://%s%s/groups/%s/memberships" % (grouper_host,
                                                             grouper_base_path, grouper_groupname), auth=(grouper_user, grouper_pass))
    grouper_resp = r.json()
    if 'wsMemberships' in grouper_resp['WsGetMembershipsResults']:
        for rec in grouper_resp['WsGetMembershipsResults']['wsMemberships']:
            if re.search('^\d{12}$', rec['subjectId']) and rec['subjectSourceId'] == "ldap" and rec['membershipType'] == "immediate":
                grouper_members[rec['subjectId']] = rec[
                    'immediateMembershipId']
except requests.exceptions.RequestException as e:
    print(e)
    exit(1)

# delete all members from Grouper not in EDS results
del_mems = set(grouper_members.keys()) - set(eds_members.keys())
for m in del_mems:
    try:
        payload = {'WsRestGetAttributeAssignmentsLiteRequest': {'wsAttributeDefNameName':
                                                                grouper_attr_name, 'attributeAssignType': 'imm_mem', 'wsOwnerMembershipId': grouper_members[m]}}
        headers = {'Content-Type': 'text/x-json; charset=UTF-8'}
        r = requests.post("https://%s%s/attributeAssignments" % (grouper_host, grouper_base_path),
                          auth=(grouper_user, grouper_pass), headers=headers, json=payload)
        grouper_resp = r.json()
        if grouper_resp['WsGetAttributeAssignmentsResults']['resultMetadata']['resultCode'] == "SUCCESS" and re.search('Found 1 result', grouper_resp['WsGetAttributeAssignmentsResults']['resultMetadata']['resultMessage']):
            aares = grouper_resp['WsGetAttributeAssignmentsResults'][
                'wsAttributeAssigns'][0]
            if aares['attributeDefNameName'] == grouper_attr_name and aares['enabled'] == 'T' and aares['wsAttributeAssignValues'][0]['valueSystem'] not in searchTerms.keys():
                # this learner was added under a previous term and membership
                # hasn't expired yet...skip and continue
                continue

        # otherwise, delete membership from group, as person is no longer a
        # learner
        r = requests.delete("https://%s%s/groups/%s/members/%s" % (grouper_host,
                                                                   grouper_base_path, grouper_groupname, m), auth=(grouper_user, grouper_pass))
        grouper_resp = r.json()
        if grouper_resp['WsDeleteMemberLiteResult']['resultMetadata']['resultCode'] not in ('SUCCESS', 'SUCCESS_WASNT_IMMEDIATE_BUT_HAS_EFFECTIVE'):
            print("Error deleting %s from group %s : %s" % (m, grouper_resp['WsDeleteMemberLiteResult']['wsGroup']['name'], grouper_resp['WsDeleteMemberLiteResult']['resultMetadata']['resultCode']))
            exit(1)
        else:
            logEvent('DEL', grouper_groupname, m)
    except requests.exceptions.RequestException as e:
        print(e)
        exit(1)

# add all missing members to Grouper
add_mems = set(eds_members.keys()) - set(grouper_members.keys())
for m in add_mems:
    try:
        # add member to group
        payload = {'WsRestAddMemberLiteRequest': {'groupName': grouper_groupname, 'subjectId': m, 'disabledTime': (
            eds_members[m]['endDate'] + timedelta(days=28)).strftime("%Y/%m/%d %H:%M:%S.%f")[:-3]}}
        headers = {'Content-Type': 'text/x-json; charset=UTF-8'}
        r = requests.post("https://%s%s/groups/%s/members" % (grouper_host, grouper_base_path,
                                                              grouper_groupname), auth=(grouper_user, grouper_pass), headers=headers, json=payload)
        grouper_resp = r.json()

        if grouper_resp['WsAddMemberLiteResult']['resultMetadata']['resultCode'] in ('SUCCESS', 'SUCCESS_ALREADY_EXISTED', 'SUCCESS_WASNT_IMMEDIATE_BUT_HAS_EFFECTIVE'):

            # if this is a new membership, add "memberterm" attribute
            # indicating term code
            if grouper_resp['WsAddMemberLiteResult']['resultMetadata']['resultCode'] == 'SUCCESS':
                logEvent('ADD', grouper_groupname, m)

                # get immediate membership ID for subject
                r = requests.get("https://%s%s/groups/%s/memberships" % (grouper_host,
                                                                         grouper_base_path, grouper_groupname), auth=(grouper_user, grouper_pass))
                grouper_resp = r.json()
                if 'wsMemberships' in grouper_resp['WsGetMembershipsResults']:
                    for rec in grouper_resp['WsGetMembershipsResults']['wsMemberships']:

                        # if this membership is the one for the subject, add
                        # 'memberterm' attribute
                        if rec['subjectId'] == m and rec['subjectSourceId'] == "ldap" and rec['membershipType'] == "immediate":
                            payload = {'WsRestAssignAttributesLiteRequest': {'attributeAssignOperation': 'assign_attr', 'wsAttributeDefNameName': grouper_attr_name, 'attributeAssignType': 'imm_mem',
                                                                             'attributeAssignValueOperation': 'add_value', 'valueSystem': eds_members[m]['term'], 'wsOwnerMembershipId': rec['immediateMembershipId']}}  # THIS IS THE REAL CODE
                            # payload = {'WsRestAssignAttributesLiteRequest': {'attributeAssignOperation': 'assign_attr', 'wsAttributeDefNameName': grouper_attr_name, 'attributeAssignType': 'imm_mem', 'attributeAssignValueOperation': 'add_value', 'valueSystem': '2151', 'wsOwnerMembershipId': rec['immediateMembershipId'] }}

                            r = requests.post("https://%s%s/attributeAssignments" % (grouper_host, grouper_base_path), auth=(
                                grouper_user, grouper_pass), headers=headers, json=payload)
                            grouper_resp = r.json()
                            if grouper_resp['WsAssignAttributesLiteResults']['resultMetadata']['resultCode'] != 'SUCCESS':
                                print("Error adding attribute for %s to group membership %s : %s" % (m, grouper_groupname, grouper_resp['   WsAssignAttributesLiteResults']['resultMetadata']['resultCode']))
                                exit(1)
                            break
        else:
            print("Error adding %s to group %s : %s" % (m, grouper_resp['WsAddMemberLiteResult']['wsGroup']['name'], grouper_resp['WsAddMemberLiteResult']['resultMetadata']['resultCode']))
            exit(1)
    except requests.exceptions.RequestException as e:
        print(e)
        exit(1)
