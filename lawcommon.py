import os
import ldap
import smtplib
from datetime import datetime


def uaidToNetid(uaid):
    eds_user = os.environ['EDS_USER']
    eds_base_dn = os.environ['EDS_BASE_DN']
    eds_bind_dn = "uid=" + eds_user + ",ou=app users," + eds_base_dn
    eds_bind_pw = os.environ['EDS_PW']
    eds_search_dn = "ou=people," + eds_base_dn
    eds_host = os.environ['EDS_HOST']

    ld = ldap.initialize(eds_host)
    try:
        # get eligible members from EDS
        uid = ""
        ld.simple_bind_s(eds_bind_dn, eds_bind_pw)
        res = ld.search_s(eds_search_dn, ldap.SCOPE_SUBTREE, "(uaid=" + uaid + ")", ['uid'])
        if len(res) == 1 and 'uid' in res[0][1]:
            uid = res[0][1]['uid'][0]
        return uid
    finally:
        ld.unbind()


def logEvent(event, group, uaid):
    script_dir = os.environ['SCRIPT_DIR']
    # open audit file
    audit = open(script_dir + '/audit.csv', 'a')
    print >>audit, "%s,%s,%s,%s" % (
        datetime.now(), event, group, uaidToNetid(uaid))
    audit.close()

#####################################
# send error notification via email #
#####################################


def send_err_email(message):
    # email params
    err_sender = os.environ['EMAIL_ERR_SENDER']
    err_recipients = os.environ['EMAIL_ERR_RCPTS'].split(',')
    err_smtp_gateway = os.environ['EMAIL_SMTP_GATEWAY']

    sender = err_sender
    receivers = err_recipients

    msg = """From: Law-Grouper Sync <%s>
To: Law-Grouper Sync <%s>
Subject: Law/Grouper sync error

%s
""" % (sender, sender, message)

    try:
        smtpObj = smtplib.SMTP(err_smtp_gateway)
        smtpObj.sendmail(sender, receivers, msg)
    except smtplib.SMTPException as e:
        print "Error: unable to send email: " + e
