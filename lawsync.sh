#!/bin/sh

# where the scripts are located
export SCRIPT_DIR=/Users/gary/law_groups

# EDS config items
export EDS_USER="username"
export EDS_PW="password"
export EDS_BASE_DN="dc=eds,dc=arizona,dc=edu"
export EDS_HOST="ldaps://eds.arizona.edu"

# Grouper WS stuff
export GROUPER_WS_HOST="grouper.arizona.edu"

# email error notification stuff
export EMAIL_ERR_SENDER="sender@somewhere.arizona.edu"
export EMAIL_ERR_RCPTS="rcpt1@somewhere.arizona.edu,rcpt2@somewhere.arizona.edu"
export EMAIL_SMTP_GATEWAY="smtpgate.email.arizona.edu"

# CA chain needed for Grouper HTTPS
export REQUESTS_CA_BUNDLE=${SCRIPT_DIR}/ca_chain.pem

${SCRIPT_DIR}/law-empdcc.sh
if [ $? -ne 0 ]; then
  echo "law-empdcc.sh exited with error"
  exit 1
fi
${SCRIPT_DIR}/lawclinic-employees.sh
if [ $? -ne 0 ]; then
  echo "lawclinic-employees.sh exited with error"
  exit 1
fi
${SCRIPT_DIR}/lawclinic-students.sh
if [ $? -ne 0 ]; then
  echo "lawclinic-students.sh exited with error"
  exit 1
fi

# upload audit file to Box
${SCRIPT_DIR}/law-audit-upload.sh
